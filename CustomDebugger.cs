﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorSample
{
    internal class CustomDebugger : IDebugable
    {
        public void Show(string s)
        {
            Console.WriteLine(s);
        }
    }
}

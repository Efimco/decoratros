﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorSample
{
   public  interface IDebugable
    {

        public void Show(string s);
       
    }
}

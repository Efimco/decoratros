﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace DecoratorSample
{
    class LoaderDecorator : CustomDebuggerDecorator
    {
        public IDebugable debugable;
        public static string FilePath = @"C:\Users\User\Desktop";
        public static string FileName = "DebuggerData.txt";
        public string FullPath = Path.Combine(FilePath, FileName);
        public LoaderDecorator(IDebugable _debugable) : base(_debugable)
        {
            debugable = _debugable;
        }

        public void Show(string s)
        {
            if (File.Exists(FullPath))
            {
                debugable.Show(File.ReadAllText(FullPath));
            }
            else
            {
                debugable.Show(s);
                File.WriteAllText(FullPath, s);
            }



        }
    }
}

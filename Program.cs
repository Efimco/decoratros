﻿using System;

namespace DecoratorSample
{
    class Program
    {
        static void Main(string[] args)
        {
            CustomDebugger debugger = new CustomDebugger();

            Loader(debugger);
          //  Debugger(debugger);




        }

        public static void Loader(CustomDebugger Debugger)
        {
            LoaderDecorator loader = new LoaderDecorator(Debugger);
            loader.Show("Summa lama Duma lama");
        }

        public static void Debugger(CustomDebugger Debugger)
        {
            DebuggerDecorator decorator = new DebuggerDecorator(Debugger);
            decorator.Show("Hello!");
        }
    }
}

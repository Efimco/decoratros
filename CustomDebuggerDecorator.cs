﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorSample
{
    public class CustomDebuggerDecorator : IDebugable
    {

        public IDebugable debugable;

        public CustomDebuggerDecorator(IDebugable _debugable)
        {
            debugable = _debugable;

        }
        public void Show(string s)
        {
            debugable.Show(s);
        }
    }
}
